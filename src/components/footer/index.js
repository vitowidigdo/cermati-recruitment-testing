import React, { Component } from 'react';
import style from './index.module.css';

class Footer extends Component {
  render() {
    return (
      <div className={style.footerWrapper}>
        <div className={style.footerContainer}>
          <div className={style.footerContent}>
            <div className={style.list}>© 2021 Vito Widigdo. All Rights Reserved.</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
