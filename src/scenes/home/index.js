import React, { Component } from 'react'
import style from './index.module.css';
import backgroundImage from '../../assets/image/work-desk__dustin-lee.jpg';
import logoImage from '../../assets/image/y-logo-white.png';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Footer from '../../components/footer'

export class Home extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      banner: false,
      fade: false,
      opacity: "0",
      newsLetterClose: false,
    }
  }

  componentDidMount() {
    if (typeof window !== "undefined") {
      window.onscroll = () => {
        let currentScrollPos = window.pageYOffset;
        if (currentScrollPos > 0 ) {
          this.setState({ opacity: "1" })
        }
      }
    }
  }

  newsLetterHandler = () => {
    this.setState({
      newsLetterClose: true,
    })
  }

  render() {
    const fade = this.state.fade
    console.log(this.state.newsLetterClose)
    return (
      <>
        <style type="text/css">
          {`
            .btn-flat {
              color: rgb(255,255,255);
              border-color: rgb(255,255,255);
            }
            .btn-flat:hover {
              color: rgb(0, 74, 117);
              background-color: rgb(255,255,255);
              border-color: rgb(0, 74, 117);
            }
            .btn-banner {
              margin: 30px 0 30px 0;
              padding: 5px 15px 5px 15px;
              background-color: rgb(0, 74, 117);
              color: rgb(255,255,255);
            }
            .btn-banner:hover {
              margin: 30px 0 30px 0;
              padding: 5px 15px 5px 15px;
              background-color: rgb(255,255,255) ;
              color: rgb(0, 74, 117);
            }
            @media only screen and (max-width : 480px) {
              .btn-banner {
                margin: 55px 0 55px 0;
                padding: 0px 15px 0px 15px;
                background-color: rgb(0, 74, 117);
                color: rgb(255,255,255);
              }
              .btn-banner:hover {
                margin: 40px 0 40px 0;
                padding: 5px 10px 5px 10px;
                background-color: rgb(255,255,255) ;
                color: rgb(0, 74, 117);
              }
            }
          `}
        </style>
          <div className={style.container}>
            <div className={fade ? style.fadeOut : style.banner}>
              <div className="col-6" style={{textAlign: 'justify',margin: '10px 20px 10px 0'}}>
                By accessing and using this website, you acknowledge that you have read and
                understand our Cookie Policy, Privacy Policy, and our Terms of Service.
              </div>
              <Button variant="banner" onClick={() => this.setState({ fade: true })} onAnimationEnd={() => this.setState({ fade: false })}>Got it</Button>
            </div>
            <div style={{
              backgroundImage: `url(${backgroundImage})`,
              maxWidth: '100%',
              height: '80vh',
              backgroundPosition: 'center',
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover',
              display: 'flex',
              }}>
                <div className={fade ? style.overlayFade : style.overlay} />
                <img src={logoImage} style={{
                  position: 'relative',
                  zIndex: 1024,
                  top: 0,
                  left: 0,
                  height: 60,
                  width: 60,
                  marginLeft: 20,
                  marginTop: 10,
                }} />
                <div className={style.loginWrapper}>
                  <div className="col-7" style={{zIndex: 1024, textAlign: 'center', marginRight: 50}}>
                    <h1 style={{color: 'white'}}>Hello! I'm Vito Widigdo</h1>
                    <h2 style={{color: 'white'}}>Consult, Design, and Develop Websites</h2>
                    <p style={{color: 'white'}}>Have something great in mind? Feel free to contact me.</p>
                    <p style={{color: 'white'}}>I'll help you to make it happen.</p>
                    <Button variant="flat">LET'S MAKE CONTACT</Button>
                  </div>
                </div>
              </div>
              <div className={style.row}>
                <div style={{textAlign: 'center',}}>How Can I Help You?</div>
                <div style={{textAlign: 'center',}}>
                  Our work then targeted, best practices outcomes social innovation synergy.
                  Venture philanthropy, revolutionary inclusive policymaker relief. User-centered
                  program areas scale.
                </div>
                <Row xs={1} md={2} lg={3} style={{textAlign: 'center',marginTop: '50px'}}>
                  <Col>
                    <div style={{border: '1px solid black', padding: '10px 10px 30px 10px'}}>
                      Consult
                      <i class="far fa-comments" style={{marginLeft: '300px'}}></i>
                      <div style={{textAlign: 'justify'}}>
                      Co-create, design thinking; strengthen infrastructure resist granular.
                      Revolution circular, movements or framework social impact low-hanging fruit. 
                      Save the world compelling revolutionary progress.
                      </div>
                    </div>
                    <div style={{marginTop:'10px', border: '1px solid black', padding: '10px 10px 30px 10px'}}>
                      Marketing
                      <i class="fas fa-bullhorn" style={{marginLeft: '250px'}}></i>
                      <div style={{textAlign: 'justify'}}>
                        Peaceful; vibrant paradigm, collaborative cities. Shared vocabulary agile,
                        replicable, effective altruism youth. Mobilize commitment to overcome
                        injustice resilient, uplift social transparent effective.
                      </div>
                    </div>
                  </Col>
                  <Col>
                    <div style={{border: '1px solid black', padding: '10px'}}>Design
                      <i class="fas fa-paint-brush" style={{marginLeft: '300px'}}></i>
                      <div style={{textAlign: 'justify'}}>
                        Policymaker collaborates collective impact humanitarian shared value
                        vocabulary inspire issue outcomes agile. Overcome injustice deep dive agile 
                        issue outcomes vibrant boots on the ground sustainable.
                      </div>
                    </div>
                    <div style={{marginTop:'10px',border: '1px solid black', padding: '10px 10px 25px 10px'}}>Manage
                      <i class="fas fa-sliders-h" style={{marginLeft: '300px'}}></i>
                      <div style={{textAlign: 'justify'}}>
                      Change-makers innovation or shared unit of analysis. Overcome injustice
                      outcomes strategize vibrant boots on the ground sustainable. Optimism,
                      effective altruism invest optimism corporate social.
                      </div>
                    </div>
                  </Col>
                  <Col>
                    <div style={{border: '1px solid black', padding: '10px'}}>Develop
                      <i class="fas fa-server" style={{marginLeft: '300px'}}></i>
                      <div style={{textAlign: 'justify'}}>
                      Revolutionary circular, movements a or impact framework social impact low-
                      hanging. Save the compelling revolutionary inspire progress. Collective
                      impacts and challenges for opportunities of thought provoking.
                      </div>
                    </div>
                    <div style={{marginTop:'5px', border: '1px solid black', padding: '10px'}}>Evolve
                      <i class="fas fa-chart-line" style={{marginLeft: '300px'}}></i>
                      <div style={{textAlign: 'justify'}}>
                      Activate catalyze and impact contextualize humanitarian. Unit of analysis
                      overcome injustice storytelling altruism. Thought leadership mass 
                      incarceration. Outcomes big data, fairness, social game-changer.
                      </div>
                    </div>  
                  </Col>
                </Row>
                <div>
                </div>
              </div>
            <Footer />
              <form className={this.state.opacity != 0 ? style.fadeIn : style.fadeOut} style={this.state.newsLetterClose ? {opacity: "0"} : {opacity: `${this.state.opacity}`}}>
                <div className={style.containerNewsletter}>
                  <div style={{display: 'flex',flexDirection: 'row',justifyContent: 'space-between'}}>
                    <h2>
                      Get latest updates in web technologies
                    </h2>
                    <div style={{alignItems: 'center'}} onClick={() => this.newsLetterHandler()}>
                        <i class="fas fa-times"></i>
                      </div>
                  </div>
                  <p>
                    I write articles related to web technologies, such as design trends, development
                    tools, UI/UX case studies and reviews, and more. Sign up to my newsletter to get
                    them all.
                  </p>
                </div>

                <div className={style.containerNewsletter} style={{backgroundColor: 'white'}}>
                  <input type="text" placeholder="Email address" name="mail" />
                  <input type="submit" value="Subscribe" />
                </div>
              </form>
          </div>
      </>
    )
  }
}

export default Home
