import React, {Component, Fragment} from 'react';
import history from './history';
import Home from './scenes/home'
import 'bootstrap/dist/css/bootstrap.min.css';

import {
  Switch, Route, BrowserRouter
} from 'react-router-dom';

class App extends Component {
  render() {
    window.scrollTo(0, 0);

    let route = (
      <BrowserRouter>
        <Switch>
          <Route exact history={history} path="/" component={Home}></Route>
        </Switch>
      </BrowserRouter>
    );
    return (
      <Fragment>
        {route}
      </Fragment>
    )
  }
}

export default App;
