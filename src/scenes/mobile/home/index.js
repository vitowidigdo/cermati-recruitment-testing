import React, { Component } from 'react'
import './index.module.css';
import backgroundImage from '../../../assets/image/work-desk__dustin-lee.jpg';
import logoImage from '../../../assets/image/y-logo-white.png';
import Button from 'react-bootstrap/Button';
import Footer from '../../../components/footer'

export class MobileHome extends Component {
  render() {
    return (
      <>
        <style type="text/css">
          {`
            .btn-flat {
              color: rgb(255,255,255);
              border-color: rgb(255,255,255);
            }
            .btn-flat:hover {
              color: rgb(0, 74, 117);
              background-color: rgb(255,255,255);
              border-color: rgb(0, 74, 117);
            }
            .btn-banner {
              margin: 30px 0 30px 0;
              padding: 5px 15px 5px 15px;
              background-color: rgb(0, 74, 117);
              color: rgb(255,255,255);
            }
            .btn-banner:hover {
              margin: 40px 0 40px 0;
              padding: 5px 10px 5px 10px;
              background-color: rgb(255,255,255) ;
              color: rgb(0, 74, 117);
            }
          `}
        </style>
        <div className="container">
          <div className="banner">
            <div className="col-6" style={{textAlign: 'justify',margin: '10px 20px 10px 0'}}>
              By accessing and using this website, you acknowledge that you have read and
              understand our Cookie Policy, Privacy Policy, and our Terms of Service.
            </div>
            <Button variant="banner" onClick={() => this.bannerHandler}>Got it</Button>
          </div>
          <div style={{
            backgroundImage: `url(${backgroundImage})`,
            maxWidth: '100%',
            height: '80vh',
            backgroundPosition: 'center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            display: 'flex',
            }}>
              <div className="overlay" />
              <img src={logoImage} style={{
                position: 'relative',
                zIndex: 1024,
                top: 0,
                left: 0,
                height: 60,
                width: 60,
                marginLeft: 20,
                marginTop: 10,
              }} />
              <div className="loginWrapper">
                <div className="col-7" style={{zIndex: 1024, textAlign: 'center', marginRight: 50}}>
                  <h1 style={{color: 'white'}}>Hello! I'm Vito Widigdo</h1>
                  <h2 style={{color: 'white'}}>Consult, Design, and Develop Websites</h2>
                  <p style={{color: 'white'}}>Have something great in mind? Feel free to contact me.</p>
                  <p style={{color: 'white'}}>I'll help you to make it happen.</p>
                  <Button variant="flat">LET'S MAKE CONTACT</Button>
                </div>
              </div>
            </div>
            <div className="row">
              <div style={{textAlign: 'center',}}>How Can I Help You?</div>
              <div style={{textAlign: 'center',}}>
                Our work then targeted, best practices outcomes social innovation synergy.
                Venture philanthropy, revolutionary inclusive policymaker relief. User-centered
                program areas scale.
              </div>
              <div className="col-5">
                <div>Consult</div>
                <div>Marketing</div>
              </div>
              <div className="col-5" style={{textAlign: 'center'}}>
                <div>Design</div>
                <div>Manage</div>
              </div>
              <div className="col-5">
                <div>Develop</div>
                <div>Evolve</div>
              </div>
            </div>
          <Footer />
        </div>
      </>
    )
  }
}

export default MobileHome
